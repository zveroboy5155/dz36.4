#include <iostream>
#include "DZ36.4.h"
#include <QAppLication>
#include <QSlider>
#include <QVBoxLayout>
#include <QWidget>
#include <QPixmap>
#include <QPainter>
#include <QPaintEvent>

class ColorfulCircle : public QWidget {
	Q_OBJECT
public:
	ColorfulCircle() = default;
	ColorfulCircle(QWidget *parent) {
		setParent(parent);
		setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		mYelloyCircle = QPixmap("G:\\skillbox\\DZ36.4\\yellow");
		mGreenCircle = QPixmap("G:\\skillbox\\DZ36.4\\green");
		mRedCircle = QPixmap("G:\\skillbox\\DZ36.4\\red");
		mCurrentCircle = mGreenCircle;
		setGeometry(mRedCircle.rect());
	}
	void paintEvent(QPaintEvent *e) override;
	QSize sizeHint() const override;
private:
	QPixmap mCurrentCircle;
	QPixmap mYelloyCircle;
	QPixmap mGreenCircle;
	QPixmap mRedCircle;
public slots:
	void setYellow();
	void setGreen();
	void setRed();
};

void ColorfulCircle::setYellow() {
	mCurrentCircle = mYelloyCircle;
	update();
}
void ColorfulCircle::setGreen() {
	mCurrentCircle = mGreenCircle;
	update();
}
void ColorfulCircle::setRed() {
	mCurrentCircle = mRedCircle;
	update();
}

void ColorfulCircle::paintEvent(QPaintEvent *e) {
	QPainter p(this);
	p.drawPixmap(e->rect(), mCurrentCircle);
}

QSize ColorfulCircle::sizeHint() const {
	return QSize(160, 160);
}

int main(int argc, char *argv[]) {
	QApplication app(argc, argv);
	QWidget *window = new QWidget;
	auto *slider = new QSlider(Qt::Horizontal, window);
	ColorfulCircle *circle = new ColorfulCircle(window);
	auto *layout = new QVBoxLayout(window);

	layout->addWidget(circle);
	layout->addWidget(slider);
	QObject::connect(slider, &QSlider::valueChanged, [slider, circle](int newValue)
	{ 
		if (newValue >= 0 && newValue <= 33) circle->setGreen();
		else if (newValue > 66) circle->setRed();
		else circle->setYellow();
	});
	window->resize(180, 230);
	window->move(1000, 500);
	window->show();
	app.exec();
    return 0;
}

#include <main.moc>